@extends('layout')

@section('body')
@include('error-notification')
{!! Form::open(['url'=>'/image', 'method'=>'POST', 'files'=>'true']) !!}
 
  <div class="form-group">
  <label for="">Image File</label>
  <input type="file" class="form-control" name="userfile">
  </div>
   <div class="form-group">
   	<label for="">Caption</label>
   	<input type="text" class="form-control" name="caption">
   </div>
    <div class="form-group">
    	<label for="">Description</label>
    	<textarea name="description" id="" class="form-control"></textarea>
    </div>
    <div class="form-group">
    	<button class="btn btn-primary" type="submit">Upload</button>
    	<a href="{{ url('/image') }}" class="btn btn-warning">Cancel</a>
    </div>
{!! Form::close() !!}
@stop