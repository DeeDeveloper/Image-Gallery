@extends('layout')
@section('body')
<div class="row">
	@if(count($images) > 0)
	<div class="col-md-12 text-center">
		<a href="{{ url('/image/create') }}" class="btn btn-primary">Add New Image</a>
	    <!-- //show errors -->	
	    <hr class="divider">
	   @include('error-notification')
	</div>
	@endif
	@forelse($images as $image)
	<div class="col-sm-3">
		<div class="thumbnail">
			<img src="{{asset('/uploads/'.$image->file )}}" alt="No image">
			    <h3 class="caption">{{ $image->caption }}</h3>
				<p>{!! substr($image->description, 0 , 100) !!}</p>
			<p>
				<div class="row text-center" style="padding-left:1em">
					<a href="{{ url('/image/'.$image->id .'/edit') }}" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
					<span class="pull-left">&nbsp;</span>
					{!! Form::open(['url'=>'/image/'.$image->id, 'class'=> 'pull-left']) !!}
					  {!! Form::hidden('_method', 'DELETE') !!}
					  {!! Form::button('<span  class="glyphicon glyphicon-trash"></span> Delete', ['type' => 'submit','class' => 'btn btn-danger btn-xs', 'onclick'=>'return confirm(\'Are you sure?\')',]) !!}
				    {!! Form::close() !!}
				</div>
			</p>
		</div>
	</div>

	@empty
	<p>No Images yet, <a href="{{ url('/image/create') }}">Add New?</a></p>
	@endforelse
</div>
<div align="center">{!! $images->render() !!}</div>
@stop