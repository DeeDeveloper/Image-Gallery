<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Image Gallerly</title>
	<link rel="stylesheet" href="{{ asset('app/css/bootstrap.min.css')}}">
</head>
<body>
	<div class="container">
		<h1>Laravel 5.1 Image Gallery</h1>
		<div id="body">
			@yield('body')
		</div>
	</div>
	
</body>
</html>