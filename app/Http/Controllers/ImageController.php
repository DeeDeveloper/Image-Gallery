<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\UserImage;

use Image;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = UserImage::paginate(4);
        return view('images-list')->with('images', $images);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('add-new-image');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validation =  Validator::make($request->all(),[
            'caption' => 'required',
            'description' => 'required',
            'userfile' => 'required|image|mimes:jpeg,png|min:1|max:250']);
        //check if request fails
        if($validation->fails()){

            return redirect::back()->withInput()
                 ->with('errors', $validation->errors());

        };

        $image =new UserImage;
        //upload the image
        $file =$request->file('userfile');
        $filename = str_random(6).'_'.$file->getClientOriginalExtension();
        Image::make($file)->resize(300,200)->save( public_path('uploads/' .$filename));

     
        //save image details to database
        $image->file = $filename;
        $image->caption = $request->input('caption');
        $image->description = $request->input('description');
        $image->save();




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $image = UserImage::find($id);
        return view('image-detail')->with('image', $image);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $image = UserImage::find($id);
       return view('edit-image')->with('image', $image);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate input

        $validation = Validator::make($request->all(),[

            'caption' =>'required|min:10',
            'description' => 'required',
            'userfile' => 'sometimes|images|mimes:jpeg,png|min:1|max:650'
            ]);

        if($validation->fails()){

            return redirect()->back()->withInput()
                             ->with('errors', $validation->errors());
        }
       $image = UserImage::find($id);
       if($request->hasFile('userfile')){

        $file =$request->file('userfile');
        $filename = str_random(6). '_' .$file->getClientOriginalExtension();
        Image::make($file)->resize(300,200)->save( public_path('/uploads'.$filename));
       }

       //replace data in database

       $image->caption =$request->input('caption');
       $image->description = $request->input('description');
       $image->save();

       return redirect('/')->with('message', 'You just updated an image');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = UserImage::find($id);

        $image->delete();

        return redirect('/')->with('message', 'You have deleted image'.$image->id );
    }
}
