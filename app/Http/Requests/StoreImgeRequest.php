<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreImgeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'caption' => 'required|min:10',
          'description' => 'required',
          'userfile' => 'required|image|mimes:jpeg,png|min:1|max:250'
        ];
    }
}
